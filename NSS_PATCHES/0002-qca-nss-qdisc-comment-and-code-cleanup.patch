From 9c86cb4aff44267a1f976ee2f06ac04a0c819dfc Mon Sep 17 00:00:00 2001
From: Sakthi Vignesh Radhakrishnan <sradhakr@codeaurora.org>
Date: Thu, 29 Oct 2015 11:17:38 -0700
Subject: [PATCH 2/4] [qca-nss-qdisc] comment and code cleanup

Clean up typos and mistakes in comments. Also removes a duplicate
block of code.

The preemption comment has been removed since yield() internally
calls schedule() and therefore co-operates with other tasks that
are running. Therefore CONFIG_PREEMPT is not necessary.

Change-Id: Ibd86573c20b228409f49ceb798823c6de65df808
Signed-off-by: Sakthi Vignesh Radhakrishnan <sradhakr@codeaurora.org>
---
 nss_qdisc/nss_bf.c    |  2 +-
 nss_qdisc/nss_qdisc.c | 35 +++++------------------------------
 2 files changed, 6 insertions(+), 31 deletions(-)

diff --git a/nss_qdisc/nss_bf.c b/nss_qdisc/nss_bf.c
index 7aecb95..2ba081f 100644
--- a/nss_qdisc/nss_bf.c
+++ b/nss_qdisc/nss_bf.c
@@ -129,7 +129,7 @@ static int nss_bf_change_class(struct Qdisc *sch, u32 classid, u32 parentid,
 		}
 
 		/*
-		 * Set qos_tag of parent to which the class needs to e attached to.
+		 * Set qos_tag of parent to which the class needs to be attached to.
 		 */
 		nim_attach.msg.shaper_configure.config.msg.shaper_node_config.qos_tag = q->nq.qos_tag;
 
diff --git a/nss_qdisc/nss_qdisc.c b/nss_qdisc/nss_qdisc.c
index b8ece3b..63b51d4 100644
--- a/nss_qdisc/nss_qdisc.c
+++ b/nss_qdisc/nss_qdisc.c
@@ -1166,8 +1166,7 @@ int nss_qdisc_set_default(struct nss_qdisc *nq)
 
 	/*
 	 * Wait until cleanup operation is complete at which point the state
-	 * shall become idle. NOTE: This relies on the NSS driver to be able
-	 * to operate asynchronously which means kernel preemption is required.
+	 * shall become non-idle.
 	 */
 	while (NSS_QDISC_STATE_IDLE == (state = atomic_read(&nq->state))) {
 		yield();
@@ -1259,8 +1258,7 @@ int nss_qdisc_node_attach(struct nss_qdisc *nq, struct nss_qdisc *nq_child,
 
 	/*
 	 * Wait until cleanup operation is complete at which point the state
-	 * shall become idle. NOTE: This relies on the NSS driver to be able
-	 * to operate asynchronously which means kernel preemption is required.
+	 * shall become non-idle.
 	 */
 	while (NSS_QDISC_STATE_IDLE == (state = atomic_read(&nq->state))) {
 		yield();
@@ -1347,9 +1345,7 @@ int nss_qdisc_node_detach(struct nss_qdisc *nq, struct nss_qdisc *nq_child,
 	}
 
 	/*
-	 * Wait until cleanup operation is complete at which point the state shall become idle.
-	 * NOTE: This relies on the NSS driver to be able to operate asynchronously which means
-	 * kernel preemption is required.
+	 * Wait until cleanup operation is complete at which point the state shall become non-idle.
 	 */
 	while (NSS_QDISC_STATE_IDLE == (state = atomic_read(&nq->state))) {
 		yield();
@@ -1436,8 +1432,7 @@ int nss_qdisc_configure(struct nss_qdisc *nq,
 
 	/*
 	 * Wait until cleanup operation is complete at which point the state
-	 * shall become idle. NOTE: This relies on the NSS driver to be able
-	 * to operate asynchronously which means kernel preemption is required.
+	 * shall become non-idle.
 	 */
 	while (NSS_QDISC_STATE_IDLE == (state = atomic_read(&nq->state))) {
 		yield();
@@ -1503,8 +1498,7 @@ void nss_qdisc_destroy(struct nss_qdisc *nq)
 
 	/*
 	 * Wait until cleanup operation is complete at which point the state
-	 * shall become idle. NOTE: This relies on the NSS driver to be able
-	 * to operate asynchronously which means kernel preemption is required.
+	 * shall become idle.
 	 */
 	while (NSS_QDISC_STATE_IDLE != (state = atomic_read(&nq->state))) {
 		yield();
@@ -1698,8 +1692,6 @@ int nss_qdisc_init(struct Qdisc *sch, struct nss_qdisc *nq, nss_shaper_node_type
 
 		/*
 		 * Wait until init operation is complete.
-		 * NOTE: This relies on the NSS driver to be able to operate
-		 * asynchronously which means kernel preemption is required.
 		 */
 		while (NSS_QDISC_STATE_IDLE == (state = atomic_read(&nq->state))) {
 			yield();
@@ -1846,8 +1838,6 @@ int nss_qdisc_init(struct Qdisc *sch, struct nss_qdisc *nq, nss_shaper_node_type
 
 	/*
 	 * Wait until init operation is complete.
-	 * NOTE: This relies on the NSS driver to be able to operate asynchronously which means
-	 * kernel preemption is required.
 	 */
 	nss_qdisc_info("%s: Qdisc %p (type %d): Waiting on response from NSS for "
 			"shaper assign message\n", __func__, nq->qdisc, nq->type);
@@ -1949,21 +1939,6 @@ static void nss_qdisc_basic_stats_callback(void *app_data,
 	}
 
 	/*
-	 * Get the right stats pointers based on whether it is a class
-	 * or a qdisc.
-	 */
-	if (nq->is_class) {
-		bstats = &nq->bstats;
-		qstats = &nq->qstats;
-		refcnt = &nq->refcnt;
-	} else {
-		bstats = &qdisc->bstats;
-		qstats = &qdisc->qstats;
-		refcnt = &qdisc->refcnt;
-		qdisc->q.qlen = nq->basic_stats_latest.qlen_packets;
-	}
-
-	/*
 	 * Update qdisc->bstats
 	 */
 	spin_lock_bh(&nq->lock);
-- 
1.8.2

